#! /usr/bin/env bash

which conda || (echo "Please install conda to use this package" && exit 1)

if [ -z "$(conda env list | grep -i "centernet" )" ]; then
    conda create --name centernet -y python=3.6
    sudo apt install libgtk2.0-dev
    conda activate centernet
    conda install -y -c conda-forge easydict opencv=4.1.0 progress matplotlib
    conda install -y -c anaconda cython scipy
    conda install -y pytorch=0.4.1 torchvision cuda92 -c pytorch
fi

python src/demo.py ctdet --demo images/img-heidelberg4.jpg --load_model models/ctdet_coco_dla_2x.pth


# conda deactivate
# conda env remove --name centernet
