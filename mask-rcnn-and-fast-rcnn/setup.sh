#! /usr/bin/env bash


if [ -z "$(conda env list | grep -i "mask-rcnn" )" ]; then
    conda create --name mask-rcnn -y python=2.7
    conda activate mask-rcnn
    conda install -y -c anaconda numpy=1.16.1
    conda install -y -c anaconda cudnn protobuf future
    conda install -y -c caffe2 caffe2
    conda install -y -c pytorch pytorch cuda80
    conda install -y -c conda-forge pip matplotlib pycocotools opencv
fi

./tools/infer_simple.py \
    --cfg configs/12_2017_baselines/e2e_mask_rcnn_R-101-FPN_2x.yaml \
    --output-dir ./detectron-visualizations \
    --image-ext jpg \
    --wts "https://dl.fbaipublicfiles.com/detectron/35861858/12_2017_baselines/e2e_mask_rcnn_R-101-FPN_2x.yaml.02_32_51.SgT4y1cO/output/train/coco_2014_train:coco_2014_valminusminival/generalized_rcnn/model_final.pkl" \
    demo

conda deactivate 
conda env remove --name mask-rcnn

