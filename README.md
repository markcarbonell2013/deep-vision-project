
# Deep Vision Project

### University of Heidelberg
### Tutor: Nikolai Ufer
### Prof: Bjoern Ommer
### Fabian Regnery & Marcos Carbonell
---

## Models

Our repository contains the code of four different CNN architectures:
- CenterNet [https://github.com/Duankaiwen/CenterNet](code)
- Mask-RCNN [https://github.com/facebookresearch/Detectron](code)
- Fast-RCNN [https://github.com/facebookresearch/Detectron](code)
- YOLOv4    [https://github.com/pjreddie/darknet](code)

## Architecture

Our project base directories are:
- `center-net` contains CenterNet's code
- `mask-rcnn-and-fast-rcnn` contains Mask-RCNN's and Fast-RCNN's code (Detectron repository)
- `YOLO` contains YOLOv4's code
- `dataset` contains the Waymo Open Dataset and a reader we've developed to transform the Tensorflow binaries into Tensors and JPEG images


## Setup

We use `conda` to install all the dependencies for our projects.
Everyone of the above mentioned repositories has a `setup.sh` executable which does the following:
1. Creates a conda environment with a name and a specific python version
2. Install's the projects dependencies with the correct versions
3. Deactivates the environment afterwards

In order to run them correctly you need to use the `-i` bash flag for interactive mode:
```bash
bash -i setup.sh
```


## Reproduce our results

Use the sh-script `reproduce.sh` to train our models on the Waymo Open Dataset and view a small summary of training results













